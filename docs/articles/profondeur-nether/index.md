# La profondeur du Nether 🌍

Dans le célèbre jeu Minecraft ⛏️ il est possible de se rendre dans une autre dimension nommée "Nether".

Pour se rendre dans cette dimension, il faut créer un portail avec des pierres spéciales et procéder à 1 rituel.

![gif nether](https://media4.giphy.com/media/UPCtzhs4qzAqtX6yNb/giphy.gif?cid=ecf05e47ewkx67k4mchc81qto8nvtugs4vf9kxlsdocgmh41&rid=giphy.gif&ct=g)

Et si cette "dimension" se trouvait enfaite juste sous nos pieds ?

## Contexte

Lorsque l'on va dans le nether, et que l'on revient à la surface (l'Overworld) par un autre portail, on se rend compte d'un phénomène étrange...

TODO : photo minecraft, otherworld/nether side by side

Les faits sont : 

> chaque bloc parcouru dans le Nether représentera 8 blocs dans le monde normal.

## Théorie, visuellement

En fait ça ferait plutôt sens avec le fait que le Nether soit sous terre comme on peut le voir ci-dessous !

![vue du cercle inférieur](./trigo/Trigo-complete.png)

Le cercle intérieur est plus petit, et donc plus de blocs sont parcourus à la surface

## Calculs rapide (programme collège cycle 4)

Essayons donc de savoir à quelle profondeur se trouve le Nether.

Avec un seul théorème et quelques approximations, on va y arriver, promis !

### Approximations

D'abord, on peut faire comme-si les chemins parcourus étaient droits.

![mauvaise approximation](./approximations-circles/approxim-smallterre-1km-zoom-i.png)

Et comme la Terre a le bon gout d'être une sphère très grande, quelques mètres c'est effectivement presque un chemin droit.

![bonne approximation de loin](./approximations-circles/approxim-terre-1km-zoom-o.png)
![bonne approximation de près](./approximations-circles/approxim-terre-1km-zoom-i.png)

Sur Terre, si vous voulez commencer à voir la courbure, il faut prendre un peu de hauteur et regarder à au moins quelques kilomètres.

![eau et courbure](./courbure-longueur.jpg)

### Théorème de Thalès

C'est dans le titre, on va appliquer le [théorème de Thalès](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Thal%C3%A8s).

Effectivement, grâce à notre approximation nous avons maintenant 2 triangles inclus !

![thales contexte](./thales/Thales-complete.png)

Nous voulons connaître la longueur Pn (profondeur), ou Rn (distance du centre de la terre).

Et le théorème de Thalès permet de savoir que les côtes des triangles sont proportionnelles de la sorte que :

$$
\frac{Lo}{Ln} = \frac{Rterre}{Rn}
$$

Ensuite isolez Rn en inversant les membres et en multipliant par Rn.

$$
Rn = Rterre * \frac{Ln}{Lo} = 800km
$$

$$
Pn = Rterre - Rn = 5600km
$$

Et voilà ! Pour la planète Terre, le Nether se trouverait à 5600 km de profondeur !

Rendez-vous ci-dessous pour discuter de ça.

## Calculs exacts (lycée xx)

Ici nous allons raisonner avec les circonférences des cercles :

![Schéma complet](./trigo/Trigo-complete.png)

La formule permettant de connaitre la circonférence d'un cercle est :

$$ C = 2\pi*R $$

Dans notre cas cela donne :

$$ C_T = 2\pi*R_T $$

$$ C_N = 2\pi*R_N $$

Cependant, comme nous pouvons le voir sur notre schéma, nous avons besoin uniquement de la circonférence d'un arc de cercle (L_0 et L_n). Pour cela, il faut multiplier par l'angle Θ. L'angle étant exprimé en radian, il faut le diviser par 2π. Ainsi on obtient :

$$ L_0 = 2\pi*R_T*\frac{\theta}{2\pi} = R_T*\theta $$

$$ L_N = 2\pi*R_N*\frac{\theta}{2\pi} = R_N*\theta $$

Chaque bloc parcouru dans le nether correspondant à 8 blocs dans l'overworld, on en déduit ce rapport :

$$ \frac{L_0}{L_N} = \frac{1}{8} $$

$$ \frac{R_T*\theta}{R_N*\theta} = \frac{1}{8} $$

Et on retombe ainsi sur la formule obtenu avec le théorème de Thalès :

$$ \frac{R_T}{R_N} = \frac{1}{8} $$

$$ Rn = Rterre * \frac{Ln}{Lo} = 800km $$

$$ Pn = Rterre - Rn = 5600km $$

## Plus loin

### Appréhender

5600km, c'est beaucoup ? Assez difficile de se le représenter pour nous simple humain...

Si vous deviez y descendre avec une échelle ou un escalier, au rythme effréné de 1 marche/barreau par XXsec, soit environ xx km/h (croyez-moi, c'est déjà vraiment rapide, pour avoir essayé). Sans dormir sans manger sans s'arrêter, comptez xx mois ! Encore difficile d'imaginer en fait...

Aussi, par rapport la station spatiale internationale (ISS) que nous avons lancé dans l'espace qui se trouvent à 400km d'altitude, le Nether est 14 fois plus éloigné.

330 mètres est la plongée sous-marine la plus profonde pour un humain en combinaison et bouteille, 700 mètre en utilisant un scaphandre et 10km en sous-marin.

Pourraît-on faire un trou vers le Nether ? C'est compliqué, nous avons déjà essayé mais le trou le plus profond jamais fait forré est de ..
TODO : forages déjà effectués

Mais si nous y arrivons, que verriez-nous ? Creusons-ça tout de suite dans le dernier chapitre.

### Sciences

TODO croutes/manteau/noyau de la terre, pressions/températures

