# Fabrication de la bière

La bière, on l'aime bien, mais finalement on la connaît bien mal...

Nous y voilà, prenons quelques minutes pour apprendre de cette magnifique invention !!

## Histoire

.. cervoise, utilité, conservation

## Conception

### 1. Le maltage

céréales (souvent grain d'orge) -> production d'enzymes

Création des enzymes à partir des grains d'orges. Ceci est nécessaire à la saccharification

1. trempage : dans l'eau, dizaine d'heures
2. germination : en germant, production d'enzymes
3. touraillage : séchage à 40°C et "coup de feu" à 85-105°C
4. dégermage : retrait des petites racines dû à la germination

### 2. Le concassage et l’empâtage 

Les grains sont écrasés pour en faire une pâte, puis mélangé à de l'eau.

C'est le "maische".

### 3. Le brassage / La saccharification

fabrication amidon->sucre = "saccharification"

amidon = sucres complexes
sucre = sucres simples, ils sont fermentescibles

Se produit grâce au chauffage + action/présence enzymes

### 5. L'aromatisation

Incorporation du houblon (houblonnage) et parfois des épices/plantes/miel.

### 4. La fermentation

Action des levures pour produire l'alcool.

La fermentation peut être :

* "basse" : 5-14°C et levures caractéristiques d'une descente de celles-ci en fin de fermentation
* "haute" : 15-20°C et levures caractéristiques d'une remontée de celles-ci en fin de fermentation
* "spontanée" : levures ajoutées sans action humaine, par leur présence dans l'air ambiant

La fermentation va caractériser la bière : lager, ale, pale, ...

### 5. La maturation (ou garde)

### 6. La filtration

### 7. Le conditionnement

Elle est stockée dans des réservoirs réfrigérés avec du CO2 en pression.

La pasteurisation permet de protéger la bière de contamination.

Certaines bières sont "sur lie" ou "refermentée". Dans ce cas la pasteurisation est incompatible et gazéification/carbonatation se fait dans la bouteille, puis les levures chutent dans le fond de la bouteille.

## Variétés

Blonde/Brune/IPA/Rouge/Triple

Lager/...

# WIP :

Anecdote : Axel et Houblonnière

lexique :
* malt
* houblon
* levures

réfs :
* https://fr.wikipedia.org/wiki/Fabrication_de_la_bi%C3%A8re 
* https://www.ninkasi.fr/comment-est-fabriquee-la-biere/
* https://avis-vin.lefigaro.fr/biere/biere-artisanale/o140848-fabrication-de-la-biere-les-principales-etapes-ma-biere-artisanale

vidéos :
..
